from flask.app import Flask
from flask_login import logout_user,login_user,login_required
from wtforms import StringField, HiddenField, PasswordField, FileField, IntegerField, SelectField, SelectMultipleField
from hashlib import sha256
from .app import app, db
from flask import render_template
from .ORM import *
#from .models import get_sample, get_recherche, get_author, get_book, Author, get_authors
from flask import request, url_for, redirect
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired

@app.route('/')
@login_required
def home():
     return render_template(
        "home.html",
        albums = getAlbums()
    )

@app.route('/playlist')
def playlist(username):
    return render_template(
        "playlist.html",
        username = username,
        albums = getAlbumsparuser(username)
    )

@app.route('/artists/')
@login_required
def artists():
    return render_template(
        "artists.html",
        a = getAllArtiste()
    )


@app.route('/login/', methods=("GET", "POST"))
@login_required
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or url_for("home")
            return redirect(next)
    return render_template(
        "login.html",
        form=f
    )
    
@app.route('/search/')
@login_required
def all_musics():
    return render_template(
        "search.html",
        genres = get_all_genres(),
        singers = getAllArtiste(),
        years=getallyears()
    )
@app.route('/artist/<int:id>')
@login_required
def descArtiste(id):
    return render_template(
        "artist.html",
        a = getArtiste(id)
    )
    
@app.route('/artist/edit')
@login_required
def modifArtiste():
    return "Waiting" 


@app.route('/album/show/<int:id>')
@login_required
def descrAlbum(id):
    #print(getAllArtiste())
    a=getAlbum(id)
    return render_template("album.html",title="Description de l'album",a=a)

    
NOMBRE=16
@app.route('/album/')
def allAlbum(page=1):
    page = request.args.get('page', 1, type=int)
    users_list = Album.query.paginate(page,per_page=NOMBRE)
    return render_template("albums.html",page=users_list)
@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for("home"))

@app.route('/album/modify/<int:id>',methods=("GET", "POST"))
@login_required
def modifAlbum(id):
    a=getAlbum(id)
    f = AlbumForm()
    f.singer.default = a.artiste
    f.process()
    f.id.data = a.id
    f.title.data = a.nom
    f.release.data = a.year
    f.img.data = a.img
    return render_template("edit-album.html",album=a,form=f)

@app.route("/album/delete/<int:id>",methods=("GET", "POST"))
@login_required
def supprAlbum(id):
    deleteAlbum(id)
    return redirect(url_for("home"))

@app.route("/artist/delete/<int:id>",methods=("GET", "POST"))
@login_required
def delArtiste(id):
    deleteArtiste(id)
    return redirect(url_for("home"))

@app.route("/playlist/delete/<int:id>",methods=("GET", "POST"))
def delPlaylist(id, username):
    removefromplaylist(username, id)
    return redirect(url_for("home"))

@app.route("/playlist/delete/<int:id>",methods=("GET", "POST"))
def addPlaylist(id, username):
    addtoplaylist(username, id)
    return redirect(url_for("home"))

@app.route('/album/save/', methods=("GET", "POST"))
@login_required
def saveAlbum():
    m = None
    f = AlbumForm()
    if f.validate_on_submit() or request.method == "POST":
        m = getAlbum(int(f.id.data))

        m.title = f.title.data
        m.img = f.img.data
        m.release = int(f.release.data)
        m.singer_id = int(f.singer.data)

        db.session.commit()

        return redirect(url_for('descrAlbum', id=m.id))
    m = getAlbum(int(f.id.data))
    return render_template(
        "edit-music.html",
        music = m,
        form = f
    )
@app.route('/album/new/')
@login_required
def newalbum():
    f=AlbumForm()
    return render_template("new-album.html",form=f)


@app.route('/album/new/save', methods=["GET", 'POST'])
@login_required
def save_new_album():
    f = AlbumForm()
    if f.validate_on_submit() or request.method == "POST":
        m = Album(
            img=f.img.data,
            nom=f.nom.data,
            year=f.release.data,
            artiste=int(f.singer.data)
        )
        m.genres = []
        db.session.add(m)
        db.session.commit()
        return redirect(url_for('home', id=m.id))
    return render_template("new-album.html",form=f)


@app.route('/genre/edit/')
@login_required
def modifgenre(nom):
    return render_template("edit-genre.html",n=nom)
@app.route('/artist/new/')
@login_required
def newartist():
    f=ArtistForm()
    return render_template("new-artist.html",form=f)
@app.route('/artist/save/',methods=["GET",'POST'])
@login_required
def save_artist():
    m=None
    f=ArtistForm()
    if f.validate_on_submit or request.method=="POST":
        m=Artiste(nom=f.name.data)
        db.session.add(m)
        db.session.commit()
        return redirect(url_for('home'))
    return render_template("new-artist.html", form=f)

class LoginForm (FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = Utilisateur.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.mdp else None


class RegisterForm(FlaskForm):
    username = StringField("Username")
    password = PasswordField("Password")
    cpassword = PasswordField("Confirm Password")
    error_message = None

    def not_in_db(self):
        return Utilisateur.query.get(self.username.data) == None

    def password_confirmed(self):
        m1 = sha256()
        m1.update(self.password.data.encode())
        m1 = m1.hexdigest()
        m2 = sha256()
        m2.update(self.cpassword.data.encode())
        m2 = m2.hexdigest()
        return m1 == m2
class SearchForm(FlaskForm):
    id = StringField('Genres')
    nom=StringField('Artists')
    img = StringField('year')
class AlbumForm(FlaskForm):
    id = HiddenField('id')
    nom=StringField('Nom')
    img = StringField('Music poster')
    release = IntegerField('Release year')
    #singer = SelectField('Singer', choices=[(s.id, s.nom) for s in getAllSingers()])
@app.route("/register/", methods=("GET", "POST", ))
@login_required
def register():
    f = RegisterForm()
    if f.validate_on_submit():
        if f.not_in_db():
            if f.password_confirmed():
                user = create_user(f.username.data, f.password.data)
                login_user(user)
                return redirect(url_for("home"))
            else:
                f.error_message = "Passwords not identical"
        else:
            f.error_message = "Username already taken"
    return render_template(
        "register.html",
        title="Register",
        form=f
    )


class ArtistForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])

