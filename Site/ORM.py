from sqlalchemy.sql.sqltypes import Boolean, DateTime,Integer
from sqlalchemy.orm import *
from sqlalchemy import *
from .app import db,login_manager
from flask_login import UserMixin


    
class Artiste(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom=db.Column(db.String(100))
    albums = db.relationship("Album")
    def getnom(self):
        return self.nom
    def getAlbums(self):
        return Album.query.get_or_404(self.nom)
    def getid(self):
        return self.id
    def __repr__(self):
        return self.nom +"+"+str(self.id)

association_genre_music = db.Table('association_genre_music', db.Model.metadata,
    db.Column('album_id', db.String(100), db.ForeignKey('album.id')),
    db.Column('genre_id', db.String(100), db.ForeignKey('genre.nom'))
) 

class Album(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    nom=db.Column(db.String(100))
    artiste = db.Column(db.Integer, db.ForeignKey("artiste.id"))
    artiste_ = db.relation("Artiste")

    
    #nom_genre=db.Column(db.String(100),db.ForeignKey("Genre.nom"))
    img=db.Column(db.String(1000))
    year=db.Column(db.String(100))
    genres  = db.relationship("Genre", secondary=association_genre_music, backref="musicsss")
    def __repr__(self):
        return "Album de "+self.artiste+" ayant pour nom"+self.nom+"  genre:"+self.nom_genre+"  img:"+self.img
    def getid(self):
        return self.id
    def getimg(self):
        return self.img
    def getyear(self):
        return self.year
    def getnom(self):
        return self.nom    
    def getnom_genre(self):
        return self.nom_genre 
    
   
class Genre(db.Model):
    nom=db.Column(db.String(100),primary_key=True)
    def getnom(self):
        return self.nom
#class Playlist(db.Model):
#    id=db.Column(db.Integer,primary_key=True)
#    def getid(self):
#        return self.id
#class compose(db.Model):
 #   id_playlist=db.Column(db.Integer,db.ForeignKey("playlist.id"),primary_key=True) 
  #  id_album=db.Column(db.Integer,db.ForeignKey("album.id"),primary_key=True)
class Utilisateur(db.Model,UserMixin):
    pseudo=db.Column(db.String(100),primary_key=True)
    mdp=db.Column(db.String(100))
    #mail_user=db.Column(db.String(100))




    def get_id(self):
        return self.pseudo

    def getmdp(self):
        return self.mdp

class possede(db.Model):
    id_album = db.Column(db.Integer, db.ForeignKey("album.id"))
    pseudo=db.Column(db.String(100),db.ForeignKey("utilisateur.pseudo"))
    id_playlist=db.Column(db.Integer,primary_key=True)


    def getid(self):
        return self.id_album

    def getpseudo(self):
        return self.pseudo

    def getid(self):
        return self.id_album

    def getgenre(self):
        return self.nom_genre 

def get_playlist_by_id(self, id_playlist):
    return possede.query.get_or_404(id_playlist)

def addtoplaylist(username, id):
    ajout = possede(pseudo=username, id_album=id)
    db.session.add(ajout)
    db.session.commit()
    return ajout

def deleteAlbum(id):
    db.session.delete(getAlbum(id))
    db.session.commit()

def deleteArtiste(id):
    obj = Album.query.filter_by(artiste = id).all()
    for o in obj:
        db.session.delete(o)
        db.session.commit()
    db.session.delete(getArtiste(id))
    db.session.commit()

def removefromplaylist(username, id):
    playlist = possede.query.filter_by(username=username, id_album=id).first()
    id_playlist = possede.id_playlist
    playlist_supp=get_playlist_by_id(id_playlist)
    db.session.delete(playlist_supp)
    return playlist_supp
def get_all_genres():
    return Genre.query.all()
def getAlbums():
    return Album.query.all()
def getAllSingers():
    return Artiste.query.all()

def getAlbum(id):
    return Album.query.get_or_404(id)

def get_user(pseudo):
    return Utilisateur.query.get_or_404(pseudo)
def get_genre(nom):
    return Genre.query.get(nom)
def getArtiste(id):
    return Artiste.query.get(id)
def getAllArtiste():
    return Artiste.query.all()
def getAlbumsparuser(pseudo):
    id=possede.query.get_or_404(pseudo)
    l=[]
    for i in id:
        l.append(Album.query.get_or_404(i))
    return l
def getAllSingers():
    return Artiste.query.all()
@login_manager.user_loader
def load_user(pseudo):
    return Utilisateur.query.get(pseudo)
def getArtistebyname(name):
    return Artiste.query.get(name)


def create_user(username, password):
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    user = Utilisateur(pseudo=username, mdp=m.hexdigest())
    db.session.add(user)
    db.session.commit()
    return user
muscis_per_page = 15
def get_album_per_page(n):
    return Album.query.paginate(page=n, per_page=muscis_per_page)
def getallyears():
    A = Album.query.all()
    liste=[]
    for a in A:
        if a.year not in liste:
            liste.append(a.year)
    return liste
