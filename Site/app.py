from flask_login import LoginManager
from flask import Flask
from flask_bootstrap import Bootstrap
import os.path  # sqlalchemy
from flask_sqlalchemy import SQLAlchemy  # sqlalchemy

app = Flask(__name__)
app.run(debug=True)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

#sqlalchemy


def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))


app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

app.config["SECRET_KEY"] = "Mikasa"

#flask-login
login_manager = LoginManager(app)
login_manager.login_view = "login"

