from flask.globals import session
import yaml
import click
from .app import app, db
from sqlalchemy import func, text


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    from .ORM import Artiste,Genre,getArtiste,Album
    db.create_all()
    albumsyml=yaml.load(open(filename))
    artiste = []
    albums=[]
    genres=[]
    cpt=0
    for a in albumsyml:
        a2 = a["by"]
        qry = db.session.query(func.max(Artiste.id))
        cpt+=1
        if a2 not in artiste:
            wow = Artiste(id=cpt, nom=a2)
            #print("fait")
            db.session.add(wow)
            artiste.append(a2)
            db.session.commit()
            print(getArtiste(cpt))
            print(wow.getnom())
            print(wow.getid())
        id=a["entryId"]
        genre=a["genre"]
        nom=a["title"]
        img=a["img"]
        if img==None:
            img="noimage.png"
        year=str(a["releaseYear"]) 
        Eren=Album(id=id,nom=nom,artiste=cpt,img=img,year=year)
        if id not in albums:
            for g in genre:
                if g not in genres:
                    g2=Genre(nom=g)
                    Eren.genres.append(g2)
                    #g3=acommegenre(id_album=id,nom_genre=g)
                    db.session.add(g2)
                    #db.session.add(g3)
                    genres.append(g)
            db.session.add(Eren)
            albums.append(id)
        db.session.commit()
        #print(getArtiste(4))
#loaddb()         
#print(getAlbums())        


@app.cli.command()
def syncdb():
    """
    Creates all missing tables.
    """
    db. create_all()


@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    """
    Add a new user.
    """
    from .ORM import Utilisateur
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = Utilisateur(pseudo=username, mdp=m.hexdigest())
    db.session.add(u)
    db.session.commit()


@app.cli.command()
@click.argument('username')
@click.argument('newpassword')
def passwd(username, newpassword):
    from .ORM import Utilisateur
    from hashlib import sha256
    m = sha256()
    m.update(newpassword.encode())
    u = Utilisateur.query.filter_by(username=username).first()
    u.password = m.hexdigest()
    db.session.commit()
